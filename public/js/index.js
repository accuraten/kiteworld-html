$(document).ready(function(){
  $('.bxSlider').bxSlider({
      pager: false
  });
  $('.bxSliderPager').bxSlider({
      pagerCustom: '.bxPager',
      pagerSelector: '.bxPager'
  });
  $('input').iCheck();
  // Menu sticky on desktop
  userAgent = window.navigator.userAgent;
  if(/iPhone/.test(userAgent) || /iPod/.test(userAgent) || /iPad/.test(userAgent)) {
  } else {
    $(".productPageMenu").stick_in_parent()
  }

    $('.productPageReviewAutorDesktopTab').on('click', function(e)  {
      var currentAttrValue = '#' + $(this).attr('id');
      $('.productPageReviewAutorDesktopTabsContainer').children(currentAttrValue).fadeIn(400).siblings().hide();

      // Change/remove current tab to active
      $(this).addClass('productPageReviewAutorDesktopTabActive').siblings().removeClass('productPageReviewAutorDesktopTabActive');

      e.preventDefault();
    });
});
$(".cartMobileInfoButton").click(function() {
  var toggle_el = $(this).data("toggle");
  $(".cartMobileInfo").toggleClass("cartMobileInfoOpen");
});

$(".headerMobileMenuButton").click(function() {
  var toggle_el = $(this).data("toggle");
  $(".topMenu").toggleClass("topMenuOpen");
  $(".headerMobileMenuButton").toggleClass("headerMobileMenuButtonActive");
});

$(".headerMenuItemShop").click(function() {
  var toggle_el = $(this).data("toggle");
  $(".topMenuFull").toggleClass("topMenuFullOpen");
  $(".headerMenuItemShop").toggleClass("headerMenuItemShopActive");
});

$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 700);
        return false;
      }
    }
  });
});

// Menu active
$('.productPageDescription').waypoint(function (direction) {
  $('.productPageMenuItem').removeClass('productPageMenuActive')
  $('.productPageMenuItemDescription').addClass('productPageMenuActive');
});

$('.productPageReview').waypoint(function (direction) {
  $('.productPageMenuItem').removeClass('productPageMenuActive')
  $('.productPageMenuItemReview').addClass('productPageMenuActive');
});

$('.productPageFeedback').waypoint(function (direction) {
  $('.productPageMenuItem').removeClass('productPageMenuActive')
  $('.productPageMenuItemFeedback').addClass('productPageMenuActive');
});

$('.productPageTech').waypoint(function (direction) {
  $('.productPageMenuItem').removeClass('productPageMenuActive')
  $('.productPageMenuItemTech').addClass('productPageMenuActive');
});

$('.productPageVideo').waypoint(function (direction) {
  $('.productPageMenuItem').removeClass('productPageMenuActive')
  $('.productPageMenuItemVideo').addClass('productPageMenuActive');
});

$('.productPagePhoto').waypoint(function (direction) {
  $('.productPageMenuItem').removeClass('productPageMenuActive')
  $('.productPageMenuItemPhoto').addClass('productPageMenuActive');
});

$('.productPageBrand').waypoint(function (direction) {
  $('.productPageMenuItem').removeClass('productPageMenuActive')
  $('.productPageMenuItemBrand').addClass('productPageMenuActive');
});

// Close up menu
$(".productPageDescriptionHeader").click(function() {
  $(".productPageDescriptionText").slideToggle("fast,easeOutCubic");
  $(".productPageDescriptionHeader").toggleClass("productPageDescriptionHeaderActive");
});

$(".productPageReviewHeader").click(function() {
  $(".productPageReviewText").slideToggle("fast,easeOutCubic");
  $(".productPageReviewHeader").toggleClass("productPageReviewHeaderActive");
});

$(".productPageFeedbackHeader").click(function() {
  $(".productPageFeedbackText").slideToggle("fast,easeOutCubic");
  $(".productPageFeedbackHeader").toggleClass("productPageFeedbackHeaderActive");
});

$(".productPageTechHeader").click(function() {

  $(".productPageTechText").slideToggle("fast,easeOutCubic");
  $(".productPageTechHeader").toggleClass("productPageTechHeaderActive");
});

$(".productPageVideoHeader").click(function() {

  $(".productPageVideoText").slideToggle("fast,easeOutCubic");
  $(".productPageVideoHeader").toggleClass("productPageVideoHeaderActive");
});


$(".productPagePhotoHeader").click(function() {

  $(".productPagePhotoText").slideToggle("fast,easeOutCubic");
  $(".productPagePhotoHeader").toggleClass("productPagePhotoHeaderActive");
});


$(".productPageBrandHeader").click(function() {

  $(".productPageBrandText").slideToggle("fast,easeOutCubic");
  $(".productPageBrandHeader").toggleClass("productPageBrandHeaderActive");
});

